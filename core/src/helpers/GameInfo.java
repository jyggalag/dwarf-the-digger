package helpers;

public class GameInfo {

    public static final int WIDTH = 480;
    public static final int HEIGHT = 800;

    public static final int PLAYER_WIDTH = 81;
    public static final int PLAYER_HEIGHT = 57;

    public static final int LIFE_WIDTH = 42;
    public static final int LIFE_HEIGHT = 52;

    public static final int PLATFORM_WIDTH = 184;
    public static final int PLATFORM_HEIGHT = 46;

    public static final int COIN_WIDTH = 41;
    public static final int COIN_HEIGHT = 41;

    public static final int PIXELS_PER_METER = 100;

    public static final short DEFAULT = 1;
    public static final short PLAYER = 2;
    public static final short COLLECTABLES = 4;
    public static final short DESTROYED = 6;
}
