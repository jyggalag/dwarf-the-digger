package huds;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.jyggalag.dwarfthedigger.GameMain;

import helpers.GameInfo;
import scenes.MainMenu;

public class InstructionsButtons {
    private GameMain game;
    private Stage stage;
    private Viewport gameViewPort;

    private ImageButton backBtn;

    public InstructionsButtons(GameMain game) {
        this.game = game;
        gameViewPort = new FitViewport(GameInfo.WIDTH, GameInfo.HEIGHT, new OrthographicCamera());
        stage = new Stage(gameViewPort, game.getBatch());
        Gdx.input.setInputProcessor(stage);
        createAndPositionUIElements();
        stage.addActor(backBtn);
    }

    void createAndPositionUIElements() {
        backBtn = new ImageButton(new SpriteDrawable(new Sprite(new Texture("Buttons/Options/Back.png"))));

        backBtn.setPosition(17, 17, Align.bottomLeft);

        backBtn.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                game.setScreen(new MainMenu(game));
            }
        });
    }

    public Stage getStage() {
        return this.stage;
    }

}
