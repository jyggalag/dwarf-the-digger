package scenes;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.RunnableAction;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.jyggalag.dwarfthedigger.GameMain;

import helpers.GameInfo;
import helpers.GameManager;
import huds.UIHud;
import platforms.PlatformsController;
import player.Player;

public class Gameplay implements Screen, ContactListener {
    private GameMain game;

    private OrthographicCamera mainCamera;
    private Viewport gameViewport;

    private OrthographicCamera box2DCamera;
    private Box2DDebugRenderer debugRenderer;

    private World world;

    private Sprite[] bgs;
    private float lastYPosition;
    private boolean touchedForTheFirstTime;

    private UIHud hud;

    private PlatformsController platformsController;
    private Player player;
    private float lastPlayerY;


    public Gameplay(GameMain game) {
        this.game = game;
        mainCamera = new OrthographicCamera(GameInfo.WIDTH, GameInfo.HEIGHT);
        mainCamera.position.set(GameInfo.WIDTH / 2f, GameInfo.HEIGHT / 2f, 0);
        gameViewport = new StretchViewport(GameInfo.WIDTH, GameInfo.HEIGHT, mainCamera);

        box2DCamera = new OrthographicCamera();
        box2DCamera.setToOrtho(false, GameInfo.WIDTH / GameInfo.PIXELS_PER_METER,
                GameInfo.HEIGHT / GameInfo.PIXELS_PER_METER);
        box2DCamera.position.set(GameInfo.WIDTH / 2f, GameInfo.HEIGHT / 2f, 0);

        debugRenderer = new Box2DDebugRenderer();

        hud = new UIHud(game);

        world = new World(new Vector2(0, -9.8f), true);         // setting gravity
        world.setContactListener(this);                                       // gamplay class is a contact listener

        platformsController = new PlatformsController(world);
        player = platformsController.positionThePlayer(player);

        createBackgrounds();
    }

    /**
     * endless background
     */
    private void createBackgrounds() {
        bgs = new Sprite[3];

        // create the sense of background that is moving down
        for (int i = 0; i < bgs.length; i++) {
            bgs[i] = new Sprite(new Texture("Backgrounds/Game BG.png"));
            bgs[i].setPosition(0, -(i * bgs[i].getHeight()));
            lastYPosition = Math.abs(bgs[i].getY());
        }
    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        update(delta);

        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        game.getBatch().begin();

        drawBackgrounds();
        platformsController.drawPlatforms(game.getBatch());
        platformsController.drawCollectables(game.getBatch());
        player.drawPlayerIdle(game.getBatch());
        player.drawPlayerAnimation(game.getBatch());

        game.getBatch().end();

//        debugRenderer.render(world, box2DCamera.combined);

        game.getBatch().setProjectionMatrix(hud.getStage().getCamera().combined);
        hud.getStage().draw();

        // method for sequences and acctions
        hud.getStage().act();

        game.getBatch().setProjectionMatrix(mainCamera.combined);
        mainCamera.update();

        player.updatePlayer();

        world.step(Gdx.graphics.getDeltaTime(), 6, 2);
    }

    private void update(float delta) {
        checkForFirstTime();
        if (!GameManager.getInstance().isPaused) {
            handleInput(delta);
            moveCamera();
            checkBackgroundsOutOfBounds();
            platformsController.setCameraY(mainCamera.position.y);
            platformsController.createAndArrangeNewPlatforms();
            platformsController.removeOffScreenCollectables();
            checkPlayersBounds();
            countScore();
        }
    }

    void checkForFirstTime() {
        if (!touchedForTheFirstTime) {
            if (Gdx.input.justTouched()) {
                touchedForTheFirstTime = true;
                GameManager.getInstance().isPaused = false;
                lastYPosition = player.getY();
            }
        }
    }

    void handleInput(float delta) {
        if (Gdx.input.isKeyPressed(Input.Keys.A)) {
            player.movePlayer(-2);
        } else if (Gdx.input.isKeyPressed(Input.Keys.D)) {
            player.movePlayer(2);
        } else {
            player.setWalking(false);
        }
    }

    private void moveCamera() {
        mainCamera.position.y -= 1.5f;
    }

    private void checkBackgroundsOutOfBounds() {
        for (Sprite bg : bgs) {
            if ((bg.getY() - bg.getHeight() / 2f - 5) > mainCamera.position.y) {
                float newPosition = bg.getHeight() + lastYPosition;
                bg.setPosition(0, -newPosition);
                lastYPosition = Math.abs(newPosition);
            }
        }
    }

    void checkPlayersBounds() {
        if (player.getY() - GameInfo.HEIGHT / 2f - player.getHeight() / 2f
            > mainCamera.position.y) {
            if (!player.isDead()) {
                playerDied();
            }
        }
        else if (player.getY() + GameInfo.HEIGHT / 2f + player.getHeight() / 2f
                < mainCamera.position.y) {
;            if (!player.isDead()) {
                playerDied();
            }
        }

        if (player.getX() - 60 > GameInfo.WIDTH || player.getX() + 60 < 0) {
;            if (!player.isDead()) {
                playerDied();
            }
        }
    }

    void countScore() {
        if (lastPlayerY > player.getY()) {
            hud.incrementScore(1);
            lastPlayerY = player.getY();
        }
    }

    void playerDied() {
        GameManager.getInstance().isPaused = true;

        //decrement life
        hud.decrementLife();
        player.setDead(true);
        player.setPosition(1000, 1000);

        if (GameManager.getInstance().lifeScore < 0) {
            // game over


            GameManager.getInstance().checkForNEwHighscores();
            // check new highscore
            hud.createGameOverPanel();

            // load main menu
            RunnableAction runnableAction = new RunnableAction();
            runnableAction.setRunnable(new Runnable() {
                @Override
                public void run() {
                    game.setScreen(new MainMenu(game));
                }
            });

            SequenceAction sequenceAction = new SequenceAction();
            sequenceAction.addAction(Actions.delay(2f));
            sequenceAction.addAction(Actions.fadeOut(1f));
            sequenceAction.addAction(runnableAction);

            hud.getStage().addAction(sequenceAction);

        } else {
            // reload game
            RunnableAction runnableAction = new RunnableAction();
            runnableAction.setRunnable(new Runnable() {
                @Override
                public void run() {
                    game.setScreen(new Gameplay(game));
                }
            });

            SequenceAction sequenceAction = new SequenceAction();
            sequenceAction.addAction(Actions.delay(2f));
            sequenceAction.addAction(Actions.fadeOut(1f));
            sequenceAction.addAction(runnableAction);

            hud.getStage().addAction(sequenceAction);
        }
    }

    /**
     * drawing endless background
     */
    private void drawBackgrounds() {
        for (Sprite bg : bgs) {
            game.getBatch().draw(bg, bg.getX(), bg.getY());
        }
    }



    @Override
    public void resize(int width, int height) {
        gameViewport.update(width, height);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        world.dispose();
        for (Sprite bg : bgs) {
            bg.getTexture().dispose();
        }
        player.getTexture().dispose();
        debugRenderer.dispose();
        hud.getStage().dispose();

    }

    @Override
    public void beginContact(Contact contact) {
        Fixture body1, body2;

        // make sure that body1 is always the Player
        if (contact.getFixtureA().getUserData() == "Player") {
            body1 = contact.getFixtureA();
            body2 = contact.getFixtureB();
        } else {
            body1 = contact.getFixtureB();
            body2 = contact.getFixtureA();
        }

        if (body1.getUserData() == "Player" && body2.getUserData() == "Coin") {
            // collided with coin
            hud.incrementCoins();
            body2.setUserData("Remove");
            platformsController.removeCollectables();
        }

        if (body1.getUserData() == "Player" && body2.getUserData() == "Life") {
            // collided with life
            hud.incrementLifes();
            body2.setUserData("Remove");
            platformsController.removeCollectables();

        }

        if (body1.getUserData() == "Player" && body2.getUserData() == "flame_4") {
            if (!player.isDead()) {
                playerDied();
            }
        }
    }

    @Override
    public void endContact(Contact contact) {

    }

    @Override
    public void preSolve(Contact contact, Manifold oldManifold) {

    }

    @Override
    public void postSolve(Contact contact, ContactImpulse impulse) {

    }
}
