package platforms;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;

import java.util.Random;

import collectables.Collectables;
import helpers.GameInfo;
import helpers.GameManager;
import player.Player;

public class PlatformsController {

    private final float DISTANCE_BETWEEN_PLATFORMS = 250f;
    private float minX, maxX;
    private float lastPlatformPositionY;
    private float cameraY;

    private Random random = new Random();

    private World world;

    private Array<Platform> platforms = new Array<Platform>();
    private Array<Collectables> collectables = new Array<Collectables>();

//    private TextureAtlas platformAtlas;
    private TextureAtlas flameAtlas;
    private Animation<TextureAtlas.AtlasRegion> animation;
    private float elapsedTime;

    public PlatformsController(World world) {
        this.world = world;
        minX = GameInfo.WIDTH / 2f - 100;
        maxX = GameInfo.WIDTH / 2f + 100;
        createPlatforms();
        positionPlatforms(true);
        flameAtlas = new TextureAtlas("Platform Animation/flameAnimation.atlas");
    }

    private void createPlatforms() {
        for (int i = 0; i < 2; i++) {
            platforms.add(new Platform(world, "flame_4"));
        }
        for (int i = 0; i < 6; i++) {
            platforms.add(new Platform(world, "platform_1"));
        }

        platforms.shuffle();
    }

    private void positionPlatforms(boolean firstTImeArranging) {
        while (platforms.get(0).getPlatformName().contains("flame")) {
            platforms.shuffle();
        }

        float positionY = 0;

        if (firstTImeArranging) {
            positionY = GameInfo.HEIGHT / 2f;
        } else {
            positionY = lastPlatformPositionY;
        }

        int controlX = 0;

        for (Platform p : platforms) {
            if (p.getX() == 0 && p.getY() == 0) {
                float tempX = 0;

                if (controlX == 0) {
                    tempX = randomBetweenNumbers(minX - 10, maxX);
                    controlX = 1;
                    p.setDrawLeft(false);
                } else if (controlX == 1) {
                    tempX = randomBetweenNumbers(maxX + 10, minX);
                    controlX = 0;
                    p.setDrawLeft(true);
                }

                p.setSpritePosition(tempX, positionY);
                positionY -= DISTANCE_BETWEEN_PLATFORMS;
                lastPlatformPositionY = positionY;

                if (!firstTImeArranging && !p.getPlatformName().contains("flame")) {
                    int rand = random.nextInt(10);

                    if (rand > 5) {
                        int randomCollectable = random.nextInt(2);
                        if (randomCollectable == 0) {
                            // spawn life when life count is lower than 2
                            if (GameManager.getInstance().lifeScore < 2) {
                                Collectables collectable = new Collectables(world, "Life");
                                collectable.setCollectablePosition(p.getX(), p.getY() + 50);
                                collectables.add(collectable);
                            } else {
                                // spawn coin
                                Collectables collectable = new Collectables(world, "Coin");
                                collectable.setCollectablePosition(p.getX(), p.getY() + 50);
                                collectables.add(collectable);
                            }
                        } else {
                            // spawn coin
                            Collectables collectable = new Collectables(world, "Coin");
                            collectable.setCollectablePosition(p.getX(), p.getY() + 50);
                            collectables.add(collectable);
                        }
                    }
                }
            }
        }
    }

//    public void drawPlatforms(SpriteBatch batch) {
//        for (Platform p : platforms) {
//;           if (p.getDrawLeft()) {
//                batch.draw(p, (p.getX() - p.getHeight() / 2f) - 69, p. getY() - p.getHeight() / 2f,
//                        GameInfo.PLATFORM_WIDTH, GameInfo.PLATFORM_HEIGHT);
//            } else {
//                batch.draw(p, (p.getX() - p.getHeight() / 2f) + 69, p. getY() - p.getHeight() / 2f,
//                        GameInfo.PLATFORM_WIDTH, GameInfo.PLATFORM_HEIGHT);
//            }
//        }
//    }

    public void drawPlatforms(SpriteBatch batch) {
        for (Platform p : platforms) {
            if (p.getPlatformName().contains("platform")) {
                batch.draw(p, (p.getX() - p.getWidth() / 2f), p.getY() - p.getHeight() / 2f,
                        GameInfo.PLATFORM_WIDTH, GameInfo.PLATFORM_HEIGHT + 6);
            } else {
                elapsedTime += Gdx.graphics.getDeltaTime();
                animation = new Animation<>(1f/4f, flameAtlas.getRegions());
                batch.draw(animation.getKeyFrame(elapsedTime, true),
                        p.getX() - p.getWidth() / 2f, p.getY() - p.getHeight() / 2f, GameInfo.PLATFORM_WIDTH, GameInfo.PLATFORM_HEIGHT + 6);
            }

        }
    }

    public void drawCollectables(SpriteBatch batch) {
        for (Collectables c : collectables) {
            c.updateCollectable();
            batch.draw(c, c.getX(), c.getY());
        }
    }

    public void removeCollectables() {
        for (int i = 0; i < collectables.size; i++) {
            if (collectables.get(i).getFixture().getUserData() == "Remove") {
                collectables.get(i).changeFilter();
                collectables.get(i).getTexture().dispose();
                collectables.removeIndex(i);
            }
        }
    }

    public void createAndArrangeNewPlatforms() {
        for (int i = 0; i < platforms.size; i++) {
            if ((platforms.get(i).getY() - (GameInfo.HEIGHT >> 1) - 15) > cameraY) {
                // platform out of bounds
                platforms.get(i).getTexture().dispose();
                platforms.removeIndex(i);
            }
        }

        if (platforms.size == 4) {
            createPlatforms();
            positionPlatforms(false);
        }
    }

    public void removeOffScreenCollectables() {
        for (int i = 0; i < collectables.size; i++) {
            if ((collectables.get(i).getY() - GameInfo.HEIGHT / 2f - 15) > cameraY) {
                 collectables.get(i).getTexture().dispose();
                 collectables.removeIndex(i);
            }
        }
    }

    public void setCameraY(float cameraY) {
        this.cameraY = cameraY;
    }

    public Player positionThePlayer(Player player) {
        player = new Player(world, platforms.get(0).getX(), platforms.get(0).getY() + 55);
        return player;
    }

    private float randomBetweenNumbers(float min, float max) {
        return random.nextFloat() * (max - min) + min;
    }

}
