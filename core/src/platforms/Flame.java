package platforms;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;

import helpers.GameInfo;

public class Flame extends Sprite {

    private World world;
    private Body body;
    private String flameNAme;

    public Flame(World world, String platformName) {
        super(new Texture("Platforms/" + platformName + ".png"));
        this.world = world;
        this.flameNAme = platformName;
        setSize(GameInfo.PLATFORM_WIDTH, GameInfo.PLATFORM_HEIGHT);
    }

    void createBody() {
        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.StaticBody;

        bodyDef.position.set((getX() - 20) / GameInfo.PIXELS_PER_METER,
                getY()  / GameInfo.PIXELS_PER_METER);

        body = world.createBody(bodyDef);

        PolygonShape shape = new PolygonShape();
        shape.setAsBox((getWidth() / 2 - 16) / GameInfo.PIXELS_PER_METER, (getHeight() / 2) / GameInfo.PIXELS_PER_METER);

        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = shape;

        Fixture fixture = body.createFixture(fixtureDef);

        shape.dispose();
    }

    public void setSpritePosition(float x, float y) {
        setPosition(x, y);
        createBody();
    }

    public String getFlameNAme() {
        return this.flameNAme;
    }
}
